﻿DROP DATABASE IF EXISTS m1u1h7ej1;
CREATE DATABASE IF NOT EXISTS m1u1h7ej1;

USE m1u1h7ej1

CREATE OR REPLACE TABLE departamentos(
  cod_dpto int,
  PRIMARY KEY(cod_dpto)
);

CREATE OR REPLACE TABLE empleados(
  dni varchar(14),
  PRIMARY KEY(dni)
);

CREATE OR REPLACE TABLE pertenecen(
  departamento int,
  empleado varchar(14),
  PRIMARY KEY(departamento,empleado),
  CONSTRAINT fkPertenecenDepartamentos FOREIGN KEY (departamento)
    REFERENCES departamentos(cod_dpto) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkPertenecenEmpleados FOREIGN KEY (empleado)
    REFERENCES empleados(dni) ON DELETE CASCADE on UPDATE CASCADE
);

CREATE OR REPLACE TABLE proyectos(
  cod_proy int,
  PRIMARY KEY(cod_proy)
);


CREATE OR REPLACE TABLE trabajan(
  empleado varchar(14),
  proyecto int,
  fecha date,
  PRIMARY KEY(empleado,proyecto),
  CONSTRAINT fkTrabajanEmpleados FOREIGN KEY (empleado)
    REFERENCES empleados(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkTrabajanProyectos FOREIGN KEY (proyecto)
    REFERENCES proyectos(cod_proy) ON DELETE CASCADE ON UPDATE CASCADE
);